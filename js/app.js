const form = document.querySelector('.password-form');
const pass = document.getElementById('pass');
const confirmPass = document.getElementById('confirmPass');
const confirmBtn = document.querySelector('.btn');
const passIcon = document.getElementById('passIcon');
const confirmPassIcon = document.getElementById('confirmPassIcon');
const err = document.querySelector('.err');

function hideErrMsg() {
  err.hidden = true;
}
function showPass(el) {
  if (el.classList.contains('fa-eye')) {
    el.classList.remove('fa-eye');
    el.classList.add('fa-eye-slash');
    el.previousElementSibling.setAttribute('type', 'text');
  } else {
    el.classList.remove('fa-eye-slash');
    el.classList.add('fa-eye');
    el.previousElementSibling.setAttribute('type', 'password');
  }
}
function confirmCheck() {
  if (!pass.value || !confirmPass.value || pass.value !== confirmPass.value) {
    err.hidden = false;
  } else {
    alert('You are welcome!');
  }
}
function eventCheck(ev) {
  ev.preventDefault();
  if (ev.target === pass || ev.target === confirmPass) {
    hideErrMsg();
  }
  if (ev.target === passIcon || ev.target === confirmPassIcon) {
    showPass(ev.target);
  } 
  else if (ev.target === confirmBtn) {
    confirmCheck();
  }
}
form.addEventListener('click', eventCheck);
